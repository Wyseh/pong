"""
    Base State of the GameComponents.
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com'
__modifiers__ = u''
__date__ = u''
__version__ = u'1.0.0'


# Importations
from BasicClasses import MyBaseObject
# Specific definitions


# Classes / Functions declaration


class BaseState(MyBaseObject.BaseObject):
    """
    This will be the base GameComponents state of the game, all game state will inherit
    of this class.
    ---------------------------------------------------------------------------
    Attributes :
            - _game_view : GameView used to display the differents objects.

    """

    def __init__(self, gameView,config):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.

        """
        super(BaseState, self).__init__(config=config)
        self._gameView = gameView


    def render(self):
        """
        This method will render the current game state.
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
        """


    def actions(self):
        """
        This method will do action on the current state.
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
        """
        pass

    def getWidth(self):
        return self._gameView.getWidth()

    def getHeight(self):
        return self._gameView.getHeight()