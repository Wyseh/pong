"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'

# Importations
from BasicClasses import MyBaseObject
import pygame
import importlib


# Specific definitions


# Classes / Functions declaration


class GameView(MyBaseObject.BaseObject):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
        - _window : Window of the game.
        - _state_to_render : Current state to render on the window.
        - __game : GameComponents object that instantiated the GameView Object.
        - __viewSize : dict with (width, height)

    """

    def __init__(self, config, scene):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        """
        super(GameView, self).__init__(config=config)
        # Setting up attributes
        self.__scene = scene
        self.__stateToRender = None
        self.__viewSize = {
            "width": self._ownConfig["window"]["width"],
            "height": self._ownConfig["window"]["height"]
        }

        # init pygame
        pygame.init()

    def render(self, current_state):
        """
        This method will be used to render the current state displayed or change
        it with another one.
        -----------------------------------------------------------------------
        Arguments :
            - current_state : State to render on the window.
        -----------------------------------------------------------------------
        Return : None.
        """

        # Checking if there is a new state to render
        self.__init(current_state=current_state)

        # Rendering current state
        self.__stateToRender.render()

        # pygame.display.flip()

    def actions(self):
        self.__stateToRender.actions()

    def __init(self, current_state):
        """
        This method will be used to init a state. It takes from current_state
        as parameter to instantiate. It will create it from this parameter.
        -----------------------------------------------------------------------
        Arguments :
            - current_state : State to render on the window.
        -----------------------------------------------------------------------
        Return : None.
        """
        # Class to instantiate
        class_name = current_state.capitalize() + "State"
        # Check if the state is already displayed

        if type(self.__stateToRender).__name__ != class_name:

            # Creating dynamically the state class
            try:
                # Import library
                module = importlib.import_module("GameComponents." + class_name)
                # Get the class object
                state_class = None
                if class_name:
                    state_class = getattr(module, class_name)

                    # Setting the current state got in parameters as the new state
                    self.__stateToRender = state_class(game_view=self,
                                                       config=self._ownConfig["states"][current_state.lower()])
            except Exception as exc:
                print(
                    "Can't import : {lib}".format(lib=class_name))
                print(
                    "Error message : {msg}".format(msg=exc.args))
                self.__scene._stopEvent.set()

    def init(self):

        # init window
        self.set_up_window()

    def set_up_window(self):
        """
        This method is used to create the window of the game.
        -----------------------------------------------------------------------
        Arguments :
            - current_state : State to render on the window.
        -----------------------------------------------------------------------
        Return : None.
        """
        self.__window = pygame.display.set_mode(
            (self._ownConfig["window"]["width"], self._ownConfig["window"]["height"]))

    ## Getters // Setters ##

    def setState(self, new_state):
        self.__scene.setState(new_state)

    def getState(self):
        return self.__stateToRender
    def getWidth(self):
        return self.__viewSize["width"]

    def getHeight(self):
        return self.__viewSize["height"]

    def getWindow(self):
        return self.__window

    def isConnected(self):
        return self.__scene.isConnected()