"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'


# Importations
from BasicClasses import BasePlayer

# Specific definitions


# Classes / Functions declaration


class LocalPlayer(BasePlayer.BasePlayer):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
    
    """

    def __init__(self, config, id):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        
        """
        super(LocalPlayer, self).__init__(config=config)

        self.__inputs = {}
        self.setId(id)

    def init(self):
        self.setX( self._ownConfig["pos"]["x"])
        self.setY(self._ownConfig["pos"]["y"])
        self.setWidth(self._ownConfig["size"]["width"])
        self.setHeight(self._ownConfig["size"]["height"])
        self.setSpeed(self._ownConfig["speed"])
        self.__initiated = True
        self.setInputs(self._ownConfig["inputs"])
        self.setLocation(self._ownConfig["pos"]["location"])

    def setInputs(self, inputs):
        self.__inputs = inputs

    def getKeyLeft(self):
        return self.__inputs["code_left"]

    def getKeyRight(self):
        return self.__inputs["code_right"]
    def getInputs(self):
        return self.__inputs