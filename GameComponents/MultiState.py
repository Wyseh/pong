"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'


# Importations
from GameComponents import GameState

from ServersClients import GameClient
# Specific definitions


# Classes / Functions declaration


class MultiState(GameState.GameState):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
    
    """

    def __init__(self, config, game_view):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        
        """
        super(MultiState, self).__init__(config=config, game_view=game_view)

    def actions(self):
        """
        Method description
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
       """

    def setLocalPlayer(self, localPlayer):
        self.__players["local_player"] = localPlayer

    def setRemotePlayer(self, remotePlayer):
        self.__players["remote_player"] = remotePlayer