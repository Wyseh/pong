"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com'
__modifiers__ = u''
__date__ = u''
__version__ = u'1.0.0'


# Importations
# System library
import sys
# json library
import json

# GameComponents view class
from ServersClients import Server, GameClient

from GameComponents import Scene
# Specific definitions
ARGV_SERVER = "server"


# Classes / Functions declaration


class Main(object):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
    """
    __allConfig = {}
    __game = None
    _game_server = None

    def __init__(self):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        """
        # Initialize attributes
        # Check if it's server launch or game launch
        if(len(sys.argv) > 1):
            if(sys.argv[1] == ARGV_SERVER):
                self._game_server = None
        else:
            self.__game = None
            self.__allConfig = {}
            self.__gameClient = None



    def loadConfig(self):
        """
        This will init everything that need to be init
        -----------------------------------------------------------------------
        Arguments :

        -----------------------------------------------------------------------
        Return :
            None
        """
        # We load the config file
        cfg_file = open("./Config/config.json")
        self.__allConfig = json.load(cfg_file, encoding='utf-8')
        cfg_file.close()

    def before_launch(self):
        """
        This method will be called each time the process starts
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return :
        """
        # load config
        self.loadConfig()

        # Initialize attributes
        # Check if it's server launch or game launch
        if len(sys.argv) > 1:
            if sys.argv[1] == ARGV_SERVER:
                # Creating the server
                self._game_server = Server.Server(self.__allConfig["server"])

                # Starting the server
                self._game_server.start()
        else:
            # Creating a game object
            self.__game = Scene.Scene(config=self.__allConfig["game"], main=self)

            # Creating the game client
            self.__gameClient = GameClient.GameClient(config=self.__allConfig["game_client"])

            # Start  processes
            self.__game.start()
            self.__gameClient.start()



    def launch(self):
        """
        Main loop method of the game
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return :
            None
        """
        # Call before processing method
        self.before_launch()
        # Call after processing
        self.after_launch()

    def after_launch(self):
        """
        Thi method will be called after the game processing
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return :
            None
        """
        pass


    ## GETTERS // SETTERS ##

    def isConnected(self):
        return self.__gameClient.isConnected()
###############################################################################
# Execution ...
###############################################################################
#
#
#
# Function called when module execution is asked
if __name__ == '__main__':
    a = Main()
    a.launch()


        
    